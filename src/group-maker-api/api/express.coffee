{input} = require '../input/expressInput'
{output} = require '../output/jsonOutput'
{matcher} = require '../matcher/matcher'

class ExpressGroupMakerApiHandler
    handle: (event) ->
        users = input.getData event
        pairs = matcher.match users
        output.formatOutput pairs

handler = new ExpressGroupMakerApiHandler
exports.handler = handler