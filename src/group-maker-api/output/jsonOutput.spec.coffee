chai = require 'chai'
expect = chai.expect

{expressJsonOutputFormatter} = require './jsonOutput'

describe 'expressJsonOutputFormatter', ->
    it 'takes raw list of pairs and returns an object with pairs as items', ->
        testPairs = [['a', 'b'], ['c', 'd']]
        result = expressJsonOutputFormatter.formatOutput(testPairs)
        expect(result).to.eql({items: testPairs})
