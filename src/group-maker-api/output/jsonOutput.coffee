class ExpressJsonOutput
    formatOutput: (pairs) ->
        {items: pairs}

expressJsonOutputFormatter = new ExpressJsonOutput
exports.expressJsonOutputFormatter = expressJsonOutputFormatter
exports.output = expressJsonOutputFormatter