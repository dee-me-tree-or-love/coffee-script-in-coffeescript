chai = require 'chai'
expect = chai.expect

{randomPairMatcherService} = require './matcher'

describe 'randomPairMatcherService', ->

    it '0 users ==> returns a list of empty pairs', ->
        testUsers = []
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result).to.eql [[]]

    it '1 user ==> returns single pair of that same user', ->
        testUsers = ['a']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0].length).to.equal 2
        expect(result).to.eql [['a', 'a']]


    it '2 users ==> returns a single pair', ->
        testUsers = ['a', 'b']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0].length).to.equal 2
        expect(result[0]).to.have.members ['a', 'b']

    it '3 users ==> returns a single 3-user group', ->
        testUsers = ['a', 'b', 'c']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0].length).to.equal 3
        expect(result[0]).to.have.members ['a', 'b', 'c']

    it '4 users ==> returns a list of 2 pairs', ->
        testUsers = ['a', 'b', 'c', 'd']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0].length).to.equal 2
        expect(result[1].length).to.equal 2

    it '5 users ==> returns a list of one 3-user group and a pair', ->
        testUsers = ['a', 'b', 'c', 'd', 'e']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0].length).to.equal 3
        expect(result[1].length).to.equal 2


    it 'should connect different users', ->
        testUsers = ['a', 'b', 'c', 'd', 'e']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(result[0][0]).to.not.equal result[0][1]
        expect(result[0][0]).to.not.equal result[0][2]
        expect(result[0][1]).to.not.equal result[0][2]
        expect(result[1][0]).to.not.equal result[1][1]
        expect(result[0]).to.not.have.members result[1]

    it 'should connect every user at least once', ->
        testUsers = ['a', 'b', 'c', 'd', 'e']
        result = randomPairMatcherService.match(testUsers)
        console.debug result
        expect(
            result[0].concat result[1]
        ).to.have.members ['a', 'b', 'c', 'd', 'e']
