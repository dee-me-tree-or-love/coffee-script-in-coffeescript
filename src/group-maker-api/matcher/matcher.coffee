class SimpleRandomPairMatcher
    isEven: (users) ->
        users.length % 2 == 0

    zip: (xs, ys) ->
        xs.map (x, index) ->
            [x, ys[index]]

    shuffle: (xs) ->
        return xs unless xs.length >= 2
        for index in [xs.length-1..1]
            randomIndex = Math.floor Math.random() * (index + 1)
            [xs[index], xs[randomIndex]] = [xs[randomIndex], xs[index]]
        xs

    pair: (users) ->
        shuffledUsers = @shuffle users
        if users.length == 2
            return [shuffledUsers]
        else
            firstPair = shuffledUsers[0..shuffledUsers.length/2-1]
            secondPair = shuffledUsers[shuffledUsers.length/2..]
            @zip(firstPair, @shuffle secondPair)

    match: (users) ->
        if users.length < 2
            return [users.concat users]
        else if @isEven users
            return @pair users
        else
            pairs = @pair users[0..users.length-2]
            pairs[0] = pairs[0].concat [users[users.length-1]]
            return pairs
    


randomPairMatcherService = new SimpleRandomPairMatcher
exports.randomPairMatcherService = randomPairMatcherService
exports.matcher = randomPairMatcherService