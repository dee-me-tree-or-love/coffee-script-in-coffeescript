express = require "express"
dotenv = require "dotenv"
cors = require 'cors'

{handler} = require('./group-maker-api/api/express')

dotenv.config()

port = process.env.MATCHER_API_PORT || process.env.PORT || 3001
groupingRoute = "/groups"

app = express()
app.use express.json()
app.use cors()

app.get "/", (req, res) ->
    res.send "Go to #{groupingRoute} to create a new matching"

app.put groupingRoute, (req, res) ->
    result = handler.handle req
    res.json result

app.listen port, console.log "Server running at http://127.0.0.1:#{port}/"