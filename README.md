# Coffee Script in CoffeeScript

CoffeScript-based API for the coffee-script.  
Can be used to create random coffee-time groups.

> Making it more fun for no better reason!

## Usage

### API reference

Detailed overview of the Matcher API can be found here:
[`./doc/index.html`](./doc/index.html)

### Interface

The Matcher exposes an HTTP API that can be used to produce coffee groups:

| Path | Method | Description | Response | Request
| ---- | ------ | ----------- | -------- | -------
| `/` | `GET` | Index of the API | Welcome message | None
| `/groups` | `PUT` | Produces coffee groups form the provided input `users` list | `{"items": <(string[])[]> [["Jessica", "Pete", "Alan"], ["Phoebe", "Joana"]] }` | `json body`: `{ "users": ["Jessica", "Pete", "Joana", "Phoebe", "Alan"] }`

> NB: performance of this App is not tested beyond 100 users supplied to the
> `/groups` endpoint. If you notice issues, please open a new issue!

### Dependencies

To get started, install all the dependencies using `npm`:

```bash
$ npm i
...
added 244 packages, and audited 245 packages in 5s
...
```

### Start Server

To start the project run

```bash
$ npm run start

> coffee-script-in-coffee-script@1.0.0 start
> coffee src/index.coffee

Server running at http://127.0.0.1:3001/

```

#### Configuration

To adjust the API's settings, provide a local `.env` file.  
Contents for this file can be copied and adjusted as required
from [`.env.example`](./.env.example).  

### Development

The project is written in CoffeeScript (mainly for fun).  
More information can be found in [`./doc/`](./doc/index.html) directory.

#### Testing

To run the tests run `npm run test`

```bash
$ npm run test

> coffee-script-in-coffee-script@1.0.0 test
> mocha --require coffeescript/register "src/**/*.@(test|spec).coffee"

  group-maker-api:handler
    ✓ takes a request event and returns the matched pairs as items

...

  11 passing (39ms)
...
```

#### Linting

To lint the source code, run `npm run lint`

```bash
$ npm run lint

> coffee-script-in-coffee-script@1.0.0 lint
> coffeelint -f coffeelint.json src

  ✓ src/group-maker-api/api/express.coffee
  ✓ src/group-maker-api/api/express.spec.coffee
  ...
  ✓ src/group-maker-api/output/jsonOutput.spec.coffee
  ✓ src/index.coffee

✓ Ok! » 0 errors and 0 warnings in 9 files
```

#### Documentation

To produce new documentation, run `npm run docs`

```bash$ npm run docs

> coffee-script-in-coffee-script@1.0.0 docs
> codo src/

┌─────────┬───────┬──────────────┐
│         │ Total │ Undocumented │
├─────────┼───────┼──────────────┤
│ Files   │ 9     │              │
├─────────┼───────┼──────────────┤
│ Extras  │ 1     │              │
├─────────┼───────┼──────────────┤
| ....    | ...   | ...          |

...
  Totally documented: 0.00%
```
